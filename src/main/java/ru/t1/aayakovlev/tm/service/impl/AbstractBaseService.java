package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.AbstractEntityException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.model.AbstractModel;
import ru.t1.aayakovlev.tm.repository.BaseRepository;
import ru.t1.aayakovlev.tm.service.BaseService;

import java.util.List;

public abstract class AbstractBaseService<M extends AbstractModel, R extends BaseRepository<M>>
        implements BaseService<M> {

    protected final R repository;

    public AbstractBaseService(final R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        this.repository.clear();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public M findById(final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Override
    public M remove(final M model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        return repository.remove(model);
    }

    @Override
    public void removeAll(final List<M> models) {
        this.repository.removeAll(models);
    }

    @Override
    public M removeById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = findById(id);
        return remove(model);
    }

    @Override
    public M save(final M model) {
        if (model == null) return null;
        return repository.save(model);
    }

}
