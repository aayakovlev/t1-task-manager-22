package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;

public interface TaskService extends UserOwnedService<Task> {

    Task create(final String userId, final String name, final String description) throws AbstractFieldException;

    Task create(final String userId, final String name) throws AbstractFieldException;

    List<Task> findByProjectId(final String userId, final String projectId) throws AbstractFieldException;


}
