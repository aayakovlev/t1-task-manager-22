package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.UserNotLoggedException;
import ru.t1.aayakovlev.tm.model.User;

public interface AuthService {

    void checkRoles(Role[] roles) throws AbstractException;

    User getUser() throws AbstractException;

    String getUserId() throws UserNotLoggedException;

    boolean isAuth();

    void login(final String login, final String password) throws AbstractException;

    void logout();

    User registry(final String login, final String password, final String email) throws AbstractException;

}
