package ru.t1.aayakovlev.tm;

import ru.t1.aayakovlev.tm.component.Bootstrap;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.run(args);
        } catch (AbstractException e) {
            System.out.println(e.getMessage());
        }
    }

}
