package ru.t1.aayakovlev.tm.model;

public interface HasName {

    String getName();

    void setName(final String name);

}
