package ru.t1.aayakovlev.tm.model;

import ru.t1.aayakovlev.tm.enumerated.Status;

public final class Task extends AbstractUserOwnedModel implements WBS {

    private String projectId;

    public Task() {
    }

    public Task(final String name, final String description) {
        this.setName(name);
        this.setDescription(description);
    }

    public Task(final String name, final Status status) {
        this.setName(name);
        this.setStatus(status);
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

}
