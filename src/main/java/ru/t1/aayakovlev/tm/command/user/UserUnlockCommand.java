package ru.t1.aayakovlev.tm.command.user;

import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class UserUnlockCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Unlock user by login.";

    public static final String NAME = "user-unlock";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER UNLOCK]");
        System.out.print("Enter login: ");
        final String login = nextLine();
        getUserService().unlockUserByLogin(login);
    }

}
