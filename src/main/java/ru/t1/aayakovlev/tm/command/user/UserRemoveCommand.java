package ru.t1.aayakovlev.tm.command.user;

import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class UserRemoveCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Remove user by login.";

    public static final String NAME = "user-remove";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER REMOVE]");
        System.out.print("Enter login: ");
        final String login = nextLine();
        getUserService().removeByLogin(login);
    }

}
