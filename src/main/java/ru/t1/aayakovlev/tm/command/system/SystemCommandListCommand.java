package ru.t1.aayakovlev.tm.command.system;

import ru.t1.aayakovlev.tm.command.AbstractCommand;

import java.util.Collection;

public final class SystemCommandListCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-cmd";

    public static final String DESCRIPTION = "Show commands description.";

    public static final String NAME = "command";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMMAND LIST]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        commands.stream()
                .filter((c) -> c.getName() != null)
                .filter((c) -> !c.getName().isEmpty())
                .forEachOrdered((c) -> System.out.println(c.getName() + ": " + c.getDescription()));
    }

}
